#!/bin/env python3

import sys
import numpy
import matplotlib.pyplot as plt

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="DHE plotter")
    parser.add_argument("-o", dest="out", default=sys.stdout,
                        help="Plot csv file (default: <input file>.npy )")
    parser.add_argument("csv", help="csv file")
    cmd_args = parser.parse_args()
    out = cmd_args.out
    t, P, T_sink, T_source, * \
        T_soil = numpy.loadtxt(cmd_args.csv, delimiter=";").T

    t = t.astype("datetime64[s]")

    fig, (ax_T, ax_P) = plt.subplots(
        2, 1, gridspec_kw={"height_ratios": (4, 1)}, sharex=True)
    ax_T.set_ylabel('T [°C]')
    ax_T.plot(t, T_sink, color="#0066ff", label="T_sink")
    ax_T.plot(t, T_source, color="#ff5555", label="T_source")
    ax_T.legend()
    ax_P.fill_between(t, numpy.zeros_like(P), P, facecolor="yellow")
    ax_P.set_ylabel('P [W]')

    fig.tight_layout()
    plt.show()
