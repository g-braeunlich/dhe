# DHE - python interface

## Install

```bash
pip install [--user] dhe
```

## Build

Dependencies: rust 🦀 / cargo.

For a 🐍 optimized for your machine, just do

```bash
cd dhe_o3
cargo build --release
```

You will find a library file (`.so` on 🐧) in the `target/release`
folder.
Rename it to `dhe.so` and place into your `$PYTHONPATH` and you should
be able to import the module:

```python
import dhe
```

If you intend to build a python wheel, install [maturin](https://github.com/PyO3/maturin).
Then

```bash
cd dhe_o3
maturin build --release
```

