#![allow(non_snake_case)]

use numpy::{IntoPyArray, PyArray2, PyArray4, PyReadonlyArray1};
use pyo3::exceptions::PyRuntimeError;
use pyo3::prelude::{pyclass, pymethods, pymodule, Py, PyCell, PyErr, PyModule, PyResult, Python};
use pyo3::types::{PyAny, PySequence};
use pyo3::{FromPyObject, ToPyObject};

use libdhe::{Calculate, GParametersCore as BoundaryMethod};

fn make_error<E: std::fmt::Debug + Sized>(e: E) -> PyErr {
    PyErr::new::<PyRuntimeError, _>(format!("{:?}", e))
}

/// FromPyObject trait for foreign type
/// (we cannot implement FromPyObject for foreign types)
/// Instead FromPyObject is automatically implemented for Foreign<T: FromPyObjectForeign>
trait FromPyObjectForeign<'source>: Sized {
    fn extract_foreign(ob: &'source PyAny) -> PyResult<Self>;
}
macro_rules! impl_from_py_object_builtin {
    ($($t:ty),+) => { $(
	impl FromPyObjectForeign<'_> for $t {
	    fn extract_foreign(ob: &PyAny) -> PyResult<Self> { type T = $t; T::extract(ob) }
	}
    )* }
}
impl_from_py_object_builtin!(f64, usize, [f64; 5]);

/// Wrapper type for Foreign types, so FromPyObject can be implemented on them indirectly
struct Foreign<T>(T);
impl<'src, T: FromPyObjectForeign<'src>> FromPyObject<'src> for Foreign<T> {
    fn extract(ob: &'src PyAny) -> PyResult<Self> {
        T::extract_foreign(ob).map(Foreign)
    }
}

macro_rules! impl_from_py_object {
    ($t:ty, $($f_from:ident -> $f_to:ident),+) => {
	impl FromPyObjectForeign<'_> for $t {
	    fn extract_foreign(ob: &PyAny) -> PyResult<Self> {
                Python::with_gil(|py| {
                    let o = ob.to_object(py);
		    type T = $t;
		    let out = T{
		        $($f_to: Foreign::<_>::extract(&o.getattr(py, stringify!($f_from))?.as_ref(py))?.0,)*
		    };
                    Ok(out)
                })
	    }
	}
    }
}
impl_from_py_object!(libdhe::model::MaterialProperties, rho->rho, c -> c, lambda_ -> lambda);
impl_from_py_object!(libdhe::FluidProperties, rho -> rho, c -> c, lambda_ -> lambda, nu -> nu);
impl_from_py_object!(libdhe::SoilLayerProperties, rho -> rho, c -> c, lambda_ -> lambda, d -> d);
impl_from_py_object!(libdhe::model::SoilParameters, T_soil_mean -> T_soil_mean, T_grad -> T_grad);
impl_from_py_object!(libdhe::model::TSoil0Parameters, d_DHE->d_DHE, g_coefs -> g_coefs);
impl_from_py_object!(libdhe::DHE, x->x, y->y, L->L, D->D, D_borehole->D_borehole, thickness->thickness, Ra->Ra, Rb->Rb, R1->R1, fill_properties->fill_properties, T_soil_0_parameters->T_soil_0_parameters, brine_properties->brine_properties, Phi_m->Phi_m);
impl_from_py_object!(libdhe::GlobalParameters, dim_ax->dim_ax, dim_rad->dim_rad, soil_layers->soil_layers, R->R, optimal_n_steps_multiplier->optimal_n_steps_multiplier, Gamma->Gamma, adiabat->adiabat, n_steps_0->n_steps_0, dt_boundary_refresh->dt_boundary_refresh, dt->dt, t0->t0, soil_parameters->soil_parameters);

impl FromPyObjectForeign<'_> for BoundaryMethod {
    fn extract_foreign(any: &PyAny) -> PyResult<Self> {
        if let Ok(g) = any.downcast::<PyCell<GFuncParameters>>() {
            Ok(BoundaryMethod::GFunc(g.borrow().wrapped.clone()))
        } else {
            Ok({
                any.downcast::<PyCell<GConeParameters>>()?;
                BoundaryMethod::GCone(libdhe::GConeParameters {})
            })
        }
    }
}

fn extract_sequence<'s, T>(seq: &'s PySequence) -> PyResult<Vec<T>>
where
    Foreign<T>: FromPyObject<'s>,
{
    let mut v = Vec::with_capacity(seq.len().unwrap_or(0));
    for item in seq.iter()? {
        v.push(Foreign::<_>::extract(item?)?.0);
    }
    Ok(v)
}
impl<'a, T> FromPyObjectForeign<'a> for Vec<T>
where
    Foreign<T>: FromPyObject<'a>,
{
    fn extract_foreign(ob: &'a PyAny) -> PyResult<Self> {
        extract_sequence(ob.downcast()?)
    }
}

#[pyclass]
#[derive(Clone)]
/// One of T_BRINE_METHOD_STATIONARY or T_BRINE_METHOD_DYNAMIC.
struct TBrineMethod {
    wrapped: libdhe::TBrineCalcMethod,
}

struct GlobalParameters {
    env: libdhe::GlobalParameters,
    T_brine_method: libdhe::TBrineCalcMethod,
    g_method: BoundaryMethod,
}

impl FromPyObject<'_> for GlobalParameters {
    fn extract(ob: &PyAny) -> PyResult<Self> {
        Python::with_gil(|py| {
            let o = ob.to_object(py);
            Ok(GlobalParameters {
                T_brine_method: o
                    .getattr(py, "T_brine_method")?
                    .extract::<TBrineMethod>(py)?
                    .wrapped,
                g_method: o
                    .getattr(py, "g_method")?
                    .extract::<Foreign<BoundaryMethod>>(py)?
                    .0,
                env: <_>::extract_foreign(ob)?,
            })
        })
    }
}

#[pyclass]
/// Method using boundary condition with g function.
struct GFuncParameters {
    wrapped: libdhe::GFuncParametersCore,
}
#[pymethods]
impl GFuncParameters {
    #[new]
    fn new(g_coefs: [f64; 6], u_min: f64, L: f64, go_const: f64) -> Self {
        Self {
            wrapped: libdhe::GFuncParametersCore {
                g_coefs,
                u_min,
                L,
                go_const,
            },
        }
    }
}

#[pyclass]
/// Method using boundary condition according to cone formula by Werner
struct GConeParameters;
#[pymethods]
impl GConeParameters {
    #[new]
    fn new() -> Self {
        Self {}
    }
}

#[pymodule]
/// Python bindings for DHE.
fn dhe(py: Python, m: &PyModule) -> PyResult<()> {
    #![allow(clippy::type_complexity)]
    #[pyfn(m)]
    #[pyo3(name = "calc_P")]
    /// Compute temeratures for sink, source and pipe for given power values `P` at times `t` and parameters `dhe` / `env`.
    fn calc_P(
        py: Python,
        t: PyReadonlyArray1<f64>,
        P: PyReadonlyArray1<f64>,
        dhe: Foreign<Vec<libdhe::DHE>>,
        env: GlobalParameters,
        precision: f64,
    ) -> PyResult<(Py<PyArray2<f64>>, Py<PyArray2<f64>>, Py<PyArray4<f64>>)> {
        let GlobalParameters {
            env,
            g_method,
            T_brine_method,
        } = env;
        let dim_t = P.len();
        let dhe = dhe.0;
        let n_DHE: usize = dhe.len();
        let dim_ax = env.dim_ax;
        let dim_rad = env.dim_rad;

        let mut out_T_sink = vec![0.; n_DHE * dim_t];
        let mut out_T_source = vec![0.; n_DHE * dim_t];
        let mut out_T_soil = vec![0.; n_DHE * dim_t * (dim_rad + 2) * dim_ax];
        let chunk_size_T_soil = dim_t * (dim_rad + 2) * dim_ax;
        let mut out: Vec<libdhe::CalcPOutputRefMut> = out_T_sink
            .chunks_exact_mut(dim_t)
            .zip(out_T_source.chunks_exact_mut(dim_t))
            .zip(out_T_soil.chunks_exact_mut(chunk_size_T_soil))
            .map(|((T_sink, T_source), T_soil)| {
                Ok(libdhe::CalcPOutputRefMut {
                    T_sink,
                    T_source,
                    T_soil,
                })
            })
            .collect::<Result<Vec<libdhe::CalcPOutputRefMut>, PyErr>>()?;
        libdhe::model::PCalculationMode { precision }
            .calculate_ref(
                t.as_slice()?,
                P.as_slice()?,
                &dhe,
                &env,
                &g_method,
                T_brine_method,
                &mut out,
            )
            .map_err(make_error)?;
        let py_out_T_sink = ndarray::Array::from_shape_vec((n_DHE, dim_t), out_T_sink)
            .unwrap()
            .into_pyarray(py);
        let py_out_T_source = ndarray::Array::from_shape_vec((n_DHE, dim_t), out_T_source)
            .unwrap()
            .into_pyarray(py);
        let py_out_T_soil =
            ndarray::Array::from_shape_vec((n_DHE, dim_t, dim_rad + 2, dim_ax), out_T_soil)
                .unwrap()
                .into_pyarray(py);
        Ok((
            py_out_T_sink.to_owned(),
            py_out_T_source.to_owned(),
            py_out_T_soil.to_owned(),
        ))
    }

    m.add(
        "T_BRINE_METHOD_DYNAMIC",
        PyCell::new(
            py,
            TBrineMethod {
                wrapped: libdhe::TBrineCalcMethod::Dynamic,
            },
        )?,
    )?;
    m.add(
        "T_BRINE_METHOD_STATIONARY",
        PyCell::new(
            py,
            TBrineMethod {
                wrapped: libdhe::TBrineCalcMethod::Stationary,
            },
        )?,
    )?;
    m.add_class::<GFuncParameters>()?;
    m.add_class::<GConeParameters>()?;
    Ok(())
}
