#[allow(unused_imports)]
#[macro_use]
extern crate metastruct_derive;
#[doc(hidden)]
pub use metastruct_derive::*;

pub struct MetaField<T> {
    pub ident: &'static str,
    pub symbol: &'static str,
    pub label: &'static str,
    pub unit: &'static str,
    pub extract_mut: Extractor<T>,
}

pub enum Extractor<T> {
    F64(fn(&mut T) -> &mut f64),
    Usize(fn(&mut T) -> &mut usize),
}

impl<T> From<fn(&mut T) -> &mut f64> for Extractor<T> {
    fn from(f: fn(&mut T) -> &mut f64) -> Self {
        Self::F64(f)
    }
}
impl<T> From<fn(&mut T) -> &mut usize> for Extractor<T> {
    fn from(f: fn(&mut T) -> &mut usize) -> Self {
        Self::Usize(f)
    }
}

pub type MetaFields<T> = &'static [MetaField<T>];

pub trait MetaStruct: Sized + 'static {
    const META_FIELDS: MetaFields<Self>;
}
