#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::all, rust_2018_idioms)]

mod enum_helpers;
mod error_bar;
mod file_dialog;
mod i18n;
mod path_buf_field;
mod style;
mod tabs;
mod widget_array;

use enum_helpers::{Discriminant, EnumMemory, IntoLabel, VariantList};
use error_bar::show_error_bar;
use file_dialog::{show_file_dialog, FileDialogMode, FileDialogState};
use i18n::{Dictionary, Localization, __};
use path_buf_field::{dir_part_exists, file_exists, show_path_buf_field, PathBufFieldState};
use tabs::ShowTabs;
use widget_array::{show_widget_by_type, TabularWidgetArray, WidgetArray};

use eframe::{egui, epi, NativeOptions};
use metastruct::MetaStruct;

#[allow(clippy::just_underscores_and_digits)]
#[cfg(not(target_arch = "wasm32"))]
fn main() -> Result<(), String> {
    let loc = Dictionary::from_env();
    let __ = |key| loc.translate(key);
    let mut args = std::env::args();
    args.next();
    let path = match (args.next(), args.next()) {
        (None, None) => None,
        (Some(path), None) => Some(path),
        _ => {
            return Err(__("Wrong number of arguments!").into());
        }
    };
    let mut app = App::new_with_dict(loc);
    if let Some(path) = path {
        app.state.cfg =
            libdhe::model::DHEConfiguration::from_file(&std::path::PathBuf::from(&path))
                .map_err(|e| e.0)?;
    }
    eframe::run_native(Box::new(app), NativeOptions::default());
    Ok(())
}

pub struct App {
    loc: Dictionary,
    state: State,
}

struct State {
    pub ui_state: UiState,
    pub cfg: libdhe::DHEConfiguration,
    pub out_path: std::path::PathBuf,
}
impl Default for State {
    fn default() -> Self {
        Self {
            ui_state: Default::default(),
            cfg: Default::default(),
            out_path: std::env::temp_dir().join("untitled.csv"),
        }
    }
}

type Action = fn(&std::path::Path, &mut libdhe::DHEConfiguration) -> Result<(), String>;

impl FileDialogMode {
    fn action(self) -> Action {
        match self {
            Self::Read => load,
            Self::Write => save,
        }
    }
}

fn load(path: &std::path::Path, cfg: &mut libdhe::DHEConfiguration) -> Result<(), String> {
    *cfg = libdhe::model::DHEConfiguration::from_file(path).map_err(|e| e.0)?;
    Ok(())
}

fn save(path: &std::path::Path, cfg: &mut libdhe::DHEConfiguration) -> Result<(), String> {
    cfg.write_to_file(path).map_err(|e| e.0)?;
    Ok(())
}

#[derive(Default)]
struct UiState {
    pub selected_tab: usize,
    pub selected_dhe: usize,
    pub g_func: EnumMemory<libdhe::model::GMethod>,
    pub calculation_mode: EnumMemory<libdhe::model::CalculationMode>,
    pub load: EnumMemory<libdhe::model::LoadType>,
    pub file_dialog_state: FileDialogState,
    pub out_path_state: PathBufFieldState,
    pub sampled_load_file_state: PathBufFieldState,
    pub error_msg: Option<String>,
}

impl epi::App for App {
    fn name(&self) -> &str {
        "DHE"
    }

    #[allow(clippy::just_underscores_and_digits)]
    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {
        let App { state, loc } = self;
        let __ = |key| loc.translate(key);

        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                // load
                if ui.button("🗁").clicked() {
                    state.ui_state.file_dialog_state.mode = FileDialogMode::Read;
                    state.ui_state.file_dialog_state.open = true;
                }
                // save
                if ui.button("💾").clicked() {
                    state.ui_state.file_dialog_state.mode = FileDialogMode::Write;
                    state.ui_state.file_dialog_state.open = true;
                }
                // calc
                ui.group(|ui| {
                    ui.set_enabled(state.ui_state.out_path_state.valid);
                    if ui.button("🖩").clicked() {
                        if let Err(error) = calc_to_file(&state.cfg, &state.out_path) {
                            state.ui_state.error_msg = Some(error);
                        }
                    }
                });
                ui.label(__("Output path:"));
                show_path_buf_field(
                    &mut state.out_path,
                    &mut state.ui_state.out_path_state,
                    dir_part_exists,
                    ui,
                );
            });
            show_error_bar(&mut state.ui_state.error_msg, loc, ui);
            let State {
                ui_state:
                    UiState {
                        file_dialog_state,
                        error_msg,
                        ..
                    },
                cfg,
                ..
            } = state;
            if let Some(path) = show_file_dialog(file_dialog_state, loc, ui) {
                if let Err(msg) = file_dialog_state.mode.action()(&path, cfg) {
                    *error_msg = Some(msg);
                }
            }
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            let mut selected_tab = state.ui_state.selected_tab;
            TABS.show(&mut selected_tab, state, loc, ui);
            state.ui_state.selected_tab = selected_tab;
        });
    }
}

const TABS: &[tabs::Tab<State>] = &[
    ("DHE", App::dhe_panel),
    ("Soil", App::soil_panel),
    ("Calculation Parameters", App::calc_panel),
];

impl App {
    fn new_with_dict(loc: Dictionary) -> Self {
        Self {
            loc,
            state: Default::default(),
        }
    }

    #[allow(clippy::just_underscores_and_digits)]
    fn dhe_panel(state: &mut State, loc: &Dictionary, ui: &mut egui::Ui) {
        let __ = |key| loc.translate(key);
        egui::containers::ScrollArea::auto_sized().show(ui, |ui| {
            let mut wa = WidgetArray::new(|dhe: &mut libdhe::DHE, ui: &mut egui::Ui| {
                dhe.tabularize("tab_dhe", loc, ui);
                ui.heading(__("Fill properties"));
                dhe.fill_properties.tabularize("tab_fill", loc, ui);
                ui.heading(__("Brine properties"));
                dhe.brine_properties.tabularize("tab_brine", loc, ui);
                ui.heading(__("Parameters to calculate initial soil temperature"));
                dhe.T_soil_0_parameters.tabularize("tab_T_soil_0", loc, ui);
                show_g_coefs_grid(
                    "g_coefs_grid_dhe",
                    &mut dhe.T_soil_0_parameters.g_coefs,
                    loc,
                    ui,
                );
            });
            wa.show(&mut state.ui_state.selected_dhe, &mut state.cfg.dhe, ui);
        });
    }
    #[allow(clippy::just_underscores_and_digits)]
    fn soil_panel(state: &mut State, loc: &Dictionary, ui: &mut egui::Ui) {
        let __ = |key| loc.translate(key);
        egui::containers::ScrollArea::auto_sized().show(ui, |ui| {
            ui.heading(__("Soil parameters"));
            state
                .cfg
                .env
                .soil_parameters
                .tabularize("tab_soil", loc, ui);
            ui.heading(__("Soil layers"));
            TabularWidgetArray {
                id: "table_soil_layers",
                loc: loc.clone(),
                fields: libdhe::SoilLayerProperties::META_FIELDS,
            }
            .show(&mut state.cfg.env.soil_layers, ui);
        });
    }
    #[allow(clippy::just_underscores_and_digits)]
    fn calc_panel(state: &mut State, loc: &Dictionary, ui: &mut egui::Ui) {
        let __ = |key| loc.translate(key);
        egui::containers::ScrollArea::auto_sized().show(ui, |ui| {
            state.cfg.env.tabularize("tab_env", loc, ui);
            show_enum_combo_box(
                &mut state.cfg.g_method,
                __("Method for computing g function"),
                &mut state.ui_state.g_func,
                loc,
                ui,
            );
            if let libdhe::model::GMethod::GFunc(gfunc) = &mut state.cfg.g_method {
                egui::containers::Frame::group(&egui::style::Style::default()).show(ui, |ui| {
                    gfunc.tabularize("tab_gfunc", loc, ui);
                    show_g_coefs_grid("g_coefs_grid_g_method", &mut gfunc.g_coefs, loc, ui);
                });
            }
            show_simple_enum_combo_box(
                &mut state.cfg.T_brine_method,
                __("Method for brine temperature"),
                loc,
                ui,
            );
            show_enum_combo_box(
                &mut state.cfg.calculation_mode,
                __("Calculation Mode"),
                &mut state.ui_state.calculation_mode,
                loc,
                ui,
            );
            match &mut state.cfg.calculation_mode {
                libdhe::model::CalculationMode::P(mode) => {
                    egui::containers::Frame::group(&egui::style::Style::default()).show(ui, |ui| {
                        mode.tabularize("tab_calc_mode_p", loc, ui);
                    });
                }
            }
            show_enum_combo_box(
                &mut state.cfg.load,
                __("Load mode (input)"),
                &mut state.ui_state.load,
                loc,
                ui,
            );
            egui::containers::Frame::group(&egui::style::Style::default()).show(ui, |ui| {
                let State { cfg, ui_state, .. } = state;
                match &mut cfg.load {
                    libdhe::model::LoadType::SampledLoad(sampled_load) => {
                        let grid = egui::Grid::new("tab_sampled_load")
                            .striped(true)
                            .spacing([40.0, 4.0])
                            .min_col_width(80.0);
                        grid.show(ui, |ui| {
                            ui.label(__("Load file"));
                            show_path_buf_field(
                                &mut sampled_load.load_file,
                                &mut ui_state.sampled_load_file_state,
                                file_exists,
                                ui,
                            );
                            ui.end_row();
                            ui.label(__("Separator"));
                            let sampled_load_separator_id =
                                ui.make_persistent_id("sampled_load_separator");
                            show_csv_separator_field(
                                &mut sampled_load.separator,
                                sampled_load_separator_id,
                                loc,
                                ui,
                            );
                            ui.end_row();
                        });
                    }
                    libdhe::model::LoadType::PMonthlyLoad(monthly_load) => {
                        monthly_load.tabularize("tab_monthly_load", loc, ui);
                        ui.heading(__("Daily runtime [h] for each month"));
                        show_array_field(
                            "tab_arr_runtimes",
                            &mut monthly_load.t_run,
                            &[
                                __("Jan"),
                                __("Feb"),
                                __("Mar"),
                                __("Apr"),
                                __("Mai"),
                                __("Jun"),
                                __("Jul"),
                                __("Aug"),
                                __("Sep"),
                                __("Oct"),
                                __("Nov"),
                                __("Dec"),
                            ],
                            ui,
                        )
                    }
                }
            });
        });
    }
}

fn calc_to_file(cfg: &libdhe::DHEConfiguration, out_path: &std::path::Path) -> Result<(), String> {
    let result = cfg.calculate().map_err(|e| e.0)?;
    let mut f = std::fs::File::create(out_path).map_err(fmt_err)?;
    result.write_csv(&mut f).map_err(|e| e.0)?;
    Ok(())
}

fn fmt_err(e: impl std::fmt::Display) -> String {
    format!("{}", e)
}

#[allow(clippy::just_underscores_and_digits)]
fn show_csv_separator_field(c: &mut char, ui_id: egui::Id, loc: &Dictionary, ui: &mut egui::Ui) {
    let __ = |key| loc.translate(key);
    egui::ComboBox::from_id_source(ui_id)
        .selected_text(format!("{:?}", *c))
        .show_ui(ui, |ui| {
            for (label, sep) in &[
                (__("Tab"), '\t'),
                (__("Comma"), ','),
                (__("Semicolon"), ';'),
                (__("Space"), ' '),
            ] {
                ui.selectable_value(c, *sep, *label);
            }
        });
}

fn show_g_coefs_grid(id: &'static str, arr: &mut [f64; 5], loc: &Dictionary, ui: &mut egui::Ui) {
    ui.label(loc.translate(__("Values of g function at ln(t/ts) = x:")));
    show_array_field(id, arr, &["x=-4", "x=-2", "x=0", "x=2", "x=3"], ui);
}

fn show_array_field(id: &'static str, arr: &mut [f64], labels: &[&str], ui: &mut egui::Ui) {
    let grid = egui::Grid::new(id)
        .striped(true)
        .spacing([10.0, 4.0])
        .min_col_width(20.0);
    grid.show(ui, |ui| {
        for x in labels {
            ui.label(*x);
        }
        ui.end_row();
        for x in arr {
            ui.add(egui::widgets::DragValue::new(x));
        }
        ui.end_row();
    });
}

trait Tabularize<T> {
    fn tabularize(&mut self, id: &'static str, loc: &Dictionary, ui: &mut egui::Ui);
}

impl<T: MetaStruct> Tabularize<T> for T {
    fn tabularize(&mut self, id: &'static str, loc: &Dictionary, ui: &mut egui::Ui) {
        let grid = egui::Grid::new(id)
            .striped(true)
            .spacing([40.0, 4.0])
            .min_col_width(80.0);
        grid.show(ui, |ui| {
            for f in Self::META_FIELDS {
                ui.label(loc.translate(f.label));
                show_widget_by_type(&f.extract_mut, self, f.unit, ui);
                ui.end_row();
            }
        });
    }
}

fn show_simple_enum_combo_box<E: VariantList + IntoLabel + PartialEq>(
    variant: &mut E,
    label: impl Into<egui::Label>,
    loc: &Dictionary,
    ui: &mut egui::Ui,
) {
    let variant_str = loc.translate((*variant).into_label());
    egui::ComboBox::from_label(label)
        .selected_text(variant_str)
        .show_ui(ui, |ui| {
            for v in E::VARIANTS {
                ui.selectable_value(variant, *v, (*v).into_label());
            }
        });
}

fn show_enum_combo_box<E: Discriminant + Clone>(
    variant: &mut E,
    label: impl Into<egui::Label>,
    state: &mut EnumMemory<E>,
    loc: &Dictionary,
    ui: &mut egui::Ui,
) where
    E::DiscriminantType: PartialEq + IntoLabel + VariantList + Into<E>,
{
    let original_discriminant = variant.discriminant();
    let mut discriminant = variant.discriminant();
    show_simple_enum_combo_box(&mut discriminant, label, loc, ui);
    if discriminant != original_discriminant {
        state.insert(original_discriminant, variant.clone());
        *variant = state
            .get(&discriminant)
            .cloned()
            .unwrap_or_else(|| discriminant.into());
    }
}

setup_enum!(libdhe::model::GMethod, GMethodDiscriminant { GFunc=> __("g function method"), GCone => __("Cone method")});
setup_enum!(libdhe::model::CalculationMode, CalculationModeDiscriminant { P => "P"});
setup_enum!(libdhe::model::LoadType, LoadTypeDiscriminant { SampledLoad => __("Sampled load from file"), PMonthlyLoad => __("Monthly")});
setup_simple_enum!(libdhe::model::TBrineCalcMethod{ Dynamic => __("dynamic"), Stationary => __("stationary") });
