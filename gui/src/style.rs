use eframe::egui;

pub const CLR_ERR: egui::Color32 = egui::Color32::from_rgb(243, 89, 47);
pub const CLR_OK: egui::Color32 = egui::Color32::GREEN;
