#[allow(type_alias_bounds)]
pub(crate) type EnumMemory<T: Discriminant> = std::collections::HashMap<T::DiscriminantType, T>;

pub(crate) trait Discriminant {
    type DiscriminantType: 'static + Copy + Eq + std::hash::Hash;
    fn discriminant(&self) -> Self::DiscriminantType;
}

pub trait VariantList: 'static + Sized + Copy {
    const VARIANTS: &'static [Self];
}
pub trait IntoLabel {
    fn into_label(self) -> &'static str;
}

#[macro_export]
macro_rules! setup_enum {
    ($enum_name: path, $discriminant: ident { $($variant: ident => $label: expr),*}) => {
        #[derive(PartialEq, Eq, Clone, Copy, Hash)]
        pub enum $discriminant {
            $($variant,)*
        }

        impl Discriminant for $enum_name {
            type DiscriminantType = $discriminant;
            fn discriminant(&self) -> Self::DiscriminantType {
                match self {
                    $(Self::$variant(_) => Self::DiscriminantType::$variant,)*
                }
            }
        }
        impl From<$discriminant> for $enum_name {
            fn from(discriminant: $discriminant) -> Self {
                type E = $enum_name;
                match discriminant {
                    $($discriminant::$variant => E::$variant(Default::default()),)*
                }
            }
        }
        setup_simple_enum!($discriminant{ $($variant => $label),*});
    };
}
#[macro_export]
macro_rules! setup_simple_enum {
    ($enum_name: ty { $($variant: ident => $label: expr),*}) => {
        impl IntoLabel for $enum_name {
            fn into_label(self) -> &'static str {
                match self {
                    $(Self::$variant => $label,)*
                }
            }
        }
        impl VariantList for $enum_name {
            const VARIANTS: &'static [Self] = &[$(Self::$variant,)*];
        }
    }
}
