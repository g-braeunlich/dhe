pub trait Localization {
    /// Gets the translated text for the given key. If there is no given translation the `key` will be returned as result.
    fn translate<'a>(&'a self, key: &'a str) -> &'a str;
}

#[derive(Default, Clone)]
pub struct Dictionary(std::collections::HashMap<String, String>);

impl Localization for Dictionary {
    fn translate<'a>(&'a self, key: &'a str) -> &'a str {
        self.0.get(key).map(|key| key as &str).unwrap_or(key)
    }
}
pub fn __<T>(x: T) -> T {
    x
}

impl Dictionary {
    pub fn from_file(f: impl AsRef<std::path::Path>) -> Result<Self, std::io::Error> {
        use std::io::BufRead;
        let f = std::fs::File::open(&f)?;
        let reader = std::io::BufReader::new(f);
        Ok(read_po(reader.lines()))
    }
    pub fn from_env() -> Self {
        std::env::var("LANG")
            .map(|mut l| {
                if l.len() >= 2 {
                    l.truncate(2);
                }
                l
            })
            .map(|lang| Self::from_code(&lang))
            .unwrap_or_else(|_| Default::default())
    }
    pub fn from_code(code: &str) -> Self {
        let mut search_paths = std::env::var("HOME")
            .or_else(|_| std::env::var("USERPROFILE"))
            .ok()
            .map(|p| {
                std::path::PathBuf::from(p).join(format!(".local/share/locale/{}/dhe.po", code))
            })
            .into_iter()
            .chain(std::iter::once_with(|| {
                std::path::PathBuf::from(format!("/usr/share/locale/{}/dhe.po", code))
            }))
            .chain(
                std::iter::from_fn(|| {
                    std::env::current_exe()
                        .map(|dir| dir.with_file_name("locale").join(format!("{}.po", code)))
                        .ok()
                })
                .take(1),
            );
        search_paths
            .find_map(|p| Self::from_file(p).ok())
            .unwrap_or_else(Default::default)
    }
}

fn read_po<E>(lines: impl Iterator<Item = Result<String, E>>) -> Dictionary {
    type State = (Option<String>, Dictionary);
    fn parse_line((key_slot, mut dic): State, line: String) -> State {
        if let (None, Some(key)) = (
            &key_slot,
            line.strip_prefix("msgid \"")
                .and_then(|l| l.trim().strip_suffix('\"')),
        ) {
            if key.is_empty() {
                return (None, dic);
            }
            (Some(key.to_string()), dic)
        } else if let (true, Some(val)) = (
            key_slot.is_some(),
            line.strip_prefix("msgstr \"")
                .and_then(|l| l.trim().strip_suffix('\"')),
        ) {
            if !val.is_empty() {
                dic.0.insert(key_slot.unwrap(), val.to_string());
            }
            (None, dic)
        } else {
            (key_slot, dic)
        }
    }
    lines
        .filter_map(|l| l.ok())
        .filter(|line| !line.starts_with(&['#', '"'][..]))
        .fold((None, Dictionary::default()), parse_line)
        .1
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_read_po() {
        let expected_dic = Dictionary(
            vec![
                ("Load".into(), "Laden".into()),
                ("Save".into(), "Speichern".into()),
            ]
            .into_iter()
            .collect(),
        );
        let lines = &[
            "# German translations for PACKAGE package.",
            "# Copyright (C) 2021 THE PACKAGE'S COPYRIGHT HOLDER",
            "# This file is distributed under the same license as the PACKAGE package.",
            "#",
            r#"msgid """#,
            r#"msgstr """#,
            r#""Project-Id-Version: PACKAGE VERSION\n""#,
            r#""Report-Msgid-Bugs-To: \n""#,
            "",
            "#: standard input:1",
            r#"msgid "Load""#,
            r#"msgstr "Laden""#,
            "",
            "#: standard input:2",
            r#"msgid "Save""#,
            r#"msgstr "Speichern""#,
        ];
        let dic = read_po(
            lines
                .iter()
                .map(|l| Result::<String, ()>::Ok(l.to_string())),
        );
        assert_eq!(dic.0, expected_dic.0);
        // With newlines
        let dic = read_po(
            lines
                .iter()
                .map(|l| Result::<String, ()>::Ok(format!("{}\n", l))),
        );
        assert_eq!(dic.0, expected_dic.0);
    }
}
