use crate::i18n::{Dictionary, Localization, __};
use crate::path_buf_field::{dir_part_exists, file_exists, show_path_buf_field, PathBufFieldState};
use eframe::egui;

#[derive(Default)]
pub struct FileDialogState {
    pub path_buf_field_state: PathBufFieldState,
    pub path: std::path::PathBuf,
    pub mode: FileDialogMode,
    pub open: bool,
}
#[derive(Clone, Copy)]
pub enum FileDialogMode {
    Read,
    Write,
}
impl Default for FileDialogMode {
    fn default() -> Self {
        Self::Read
    }
}
impl From<FileDialogMode> for &str {
    fn from(mode: FileDialogMode) -> Self {
        match mode {
            FileDialogMode::Read => __("Load"),
            FileDialogMode::Write => __("Save"),
        }
    }
}
impl FileDialogMode {
    pub fn validator(self) -> fn(&std::path::Path) -> bool {
        match self {
            Self::Read => file_exists,
            Self::Write => dir_part_exists,
        }
    }
}

pub fn show_file_dialog(
    state: &mut FileDialogState,
    loc: &Dictionary,
    ui: &mut egui::Ui,
) -> Option<std::path::PathBuf> {
    if state.open {
        if let Some(response) = show_dialog(state.mode.into(), loc, ui, |ui| {
            ui.heading(loc.translate(__("File:")));
            show_path_buf_field(
                &mut state.path,
                &mut state.path_buf_field_state,
                state.mode.validator(),
                ui,
            );
        }) {
            state.open = false;
            if let DialogResponse::Ok = response {
                return Some(state.path.clone());
            }
        }
    }
    None
}

pub enum DialogResponse {
    Ok,
    Cancel,
}

fn show_dialog(
    ok_label: &str,
    loc: &Dictionary,
    ui: &mut egui::Ui,
    add_contents: impl FnOnce(&mut egui::Ui),
) -> Option<DialogResponse> {
    let mut response = None;
    egui::menu::bar(ui, |ui| {
        add_contents(ui);
        if ui.button(loc.translate(ok_label)).clicked() {
            response = Some(DialogResponse::Ok)
        }
        if ui.button(loc.translate(__("Cancel"))).clicked() {
            response = Some(DialogResponse::Cancel)
        }
    });
    response
}
