use clap::Parser;

#[derive(Debug, clap::Parser)]
#[clap(about = "DHE command line interface")]
struct Cli {
    /// Output file
    #[clap(short, long)]
    out: Option<std::path::PathBuf>,
    /// Input file
    input: std::path::PathBuf,
}

fn main() -> Result<(), libdhe::error::Error> {
    let Cli { out, input } = Cli::parse();
    let csv_file_path = out.unwrap_or_else(|| input.with_extension("csv"));
    let mut csv_file = std::fs::File::create(csv_file_path).map_err(|e| format!("{}", e))?;
    let cfg = libdhe::model::DHEConfiguration::from_file(&input)?;
    let output = cfg.calculate()?;
    output.write_csv(&mut csv_file)
}
