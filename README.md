# DHE (Downhole Heat Exchanger)

A simulation program for downhole heat exchangers. The simulation
covers 3 domains:
* The DHE's pipe containing the brine
* A surrounding soil cylinder (domain of calculation)
* Outer soil region (realized via boundary conditions)

The decomposition into the 3 domains and their discretization is shown below:

![dhe_discretization](doc/dhe-discretization.svg)

This project was partially founded by
[s3 GmbH](https://s3-engineering.ch) (formerly Sustainable System Solutions) and is
based on the publications [^hu-eWs1-sb], [^hu-eWs2-sb].

[^hu-eWs1-sb]: [Berechnungsmodul für Erdwärmesonden](http://www.hetag.ch/download/HU_EWS1_SB.pdf)

[^hu-eWs2-sb]: [Erweiterung des Programms EWSfür Erdwärmesondenfelder](http://www.hetag.ch/download/HU_EWS2_SB.pdf)

DHE consists of the following components: A GUI, a CLI and a python interface.

## Install

Go to [packages](../-/packages) and
download the latest release for your platform (currently: 🐧, 🪟).

For the 🐍 interface, see [dhe_o3](dhe_o3/README.md).

## Usage

Just run the binaries `dhe-gui` or `dhe-cli`.

### CLI

Basic usage is

```bash
dhe-cli -o <out> <input-file>
```

This will generate the file `<out>` (a csv file). See Section Output.
`<input-file>` is supposed to be a project file (json format).

For further help, use

```bash
dhe-cli --help
```

### GUI

In the tab "Calculation Parameters", specify the input mode of the
load.
If you choose "Sampled Load from file", you have to specify a csv file
in "Load file".
This must be a ";" separated csv file with exactly two columns, without header.

- Column 1: Time [sec]
- Column 2: Power [W]

The time column has not to be equispaced - the program interpolates.

Example:
```
0;500
604800;500
1209600;500
1814400;500
2419200;500
3024000;500
3628800;458
4233600;458
4838400;458
5443200;458
6048000;375
6652800;375
7257600;375
7862400;375
8467200;292
```

### Output

Output is a csv file with hourly values.
Columns

1. Time [sec],
2. T_sink: Temperature of brine flowing into the exchanger [°C]
3. T_source: Temperature of brine flowing out of the exchanger [°C]

... Temperatures of the cylinder of soil around the bore hole (Index 1:
axial, index 2: radial)

## Build the project locally

You need rust / cargo.

```bash
cargo build --release
```

Binaries only:

```bash
cargo build --release --bins
```

The resulting binaries will be put into `target/release`.

## Bibliography
