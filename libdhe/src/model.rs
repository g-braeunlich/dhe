use crate::{csv, error};
use serde::{Deserialize, Serialize};

/// Material properties
#[derive(Clone, Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "meta", derive(metastruct::MetaStruct))]
pub struct MaterialProperties {
    /// ρ: Density [kg/m^3]
    pub rho: f64,
    /// c: Specific heat [J/kg/K]
    pub c: f64,
    /// λ: Heat conductivity [W/m/K]
    pub lambda: f64,
}

/// Backfill properties
pub type FillProperties = MaterialProperties;

impl Default for FillProperties {
    fn default() -> Self {
        Self {
            rho: 1180.,
            c: 3040.,
            lambda: 0.81,
        }
    }
}

/// Fluid properties
#[derive(Clone, Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "meta", derive(metastruct::MetaStruct))]
pub struct FluidProperties {
    /// ρ: Density [kg/m^3]
    pub rho: f64,
    /// c: Specific heat [J/kg/K]
    pub c: f64,
    /// λ: Heat conductivity [W/m/K]
    pub lambda: f64,
    /// Kinematic viscosity of brine [m^2/s]
    pub nu: f64,
}

/// Brine properties
///
/// Specific heat:
///   H2O: 4200 J/kg/K
///   33 % Etylen glycol: 3800 J/kg/K
/// Kinematic viscosity:
///   H2O: 0.00000175 m^2/s
///   33 % Ethylene glycol: ~ 0.000006 m^2/s
pub type BrineProperties = FluidProperties;

impl Default for BrineProperties {
    fn default() -> Self {
        Self {
            rho: 1050.,
            c: 3875.,
            lambda: 0.449,
            nu: 0.415E-5,
        }
    }
}

/// Soil layer properties
#[derive(Clone, Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "meta", derive(metastruct::MetaStruct))]
pub struct SoilLayerProperties {
    /// ρ: Density [kg/m^3]
    pub rho: f64,
    /// c: Specific heat [J/kg/K]
    pub c: f64,
    /// λ: Heat conductivity [W/m/K]
    pub lambda: f64,
    /// d: Thickness of layer [m]
    pub d: f64,
}

impl Default for SoilLayerProperties {
    fn default() -> Self {
        Self {
            rho: 2600.,
            c: 1000.,
            lambda: 2.,
            d: std::f64::INFINITY,
        }
    }
}

/// Parameters used to compute initial soil temperature
#[derive(Clone, Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "meta", derive(metastruct::MetaStruct))]
pub struct SoilParameters {
    /// Mean temperature of soil [°C]
    pub T_soil_mean: f64,
    /// Axial gradient of temperature of soil [K/m]
    pub T_grad: f64,
}
impl Default for SoilParameters {
    fn default() -> Self {
        Self {
            T_soil_mean: 9.8,
            T_grad: 0.03,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "meta", derive(metastruct::MetaStruct))]
pub struct TSoil0Parameters {
    /// Values of g function at ln(t/ts) = (-4, -2, 0, 2, 3)
    pub g_coefs: [f64; 5],
    /// Distance between DHE [m]
    pub d_DHE: f64,
}
impl Default for TSoil0Parameters {
    fn default() -> Self {
        Self {
            g_coefs: [4.82, 5.69, 6.29, 6.57, 6.60],
            d_DHE: 10.,
        }
    }
}
#[derive(Clone, Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "meta", derive(metastruct::MetaStruct))]
pub struct GFuncParameters {
    /// Values of g function at ln(t/ts) = (-4, -2, 0, 2, 3)
    pub g_coefs: [f64; 5],
    /// Distance between DHE [m]
    pub d_DHE: f64,
    /// Distance between DHE for g function [m]
    pub d_DHE_ref: f64,
    /// [m]
    pub d_DHE_delta: f64,
    /// Length of borehole [m]
    pub L: f64,
    pub go_const: f64,
}
impl Default for GFuncParameters {
    fn default() -> Self {
        Self {
            g_coefs: [4.82, 5.69, 6.29, 6.57, 6.60],
            d_DHE: 10.,
            d_DHE_ref: 10.,
            d_DHE_delta: 0.05,
            L: 100.,
            go_const: 6.84,
        }
    }
}

/// Boundary condition according to cone formula by Werner
#[derive(Clone, Debug, Serialize, Deserialize, Default)]
pub struct GConeParameters;

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "type", content = "arguments")]
pub enum GMethod {
    GFunc(GFuncParameters),
    /// Boundary condition according to cone formula by Werner
    GCone(GConeParameters),
}
impl Default for GMethod {
    fn default() -> Self {
        Self::GCone(GConeParameters {})
    }
}

/// Downhole heat exchanger
#[derive(Clone, Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "meta", derive(metastruct::MetaStruct))]
pub struct DHE {
    /// x coordinate [m]
    pub x: f64,
    /// y coordinate [m]
    pub y: f64,
    /// L: Length [m]
    pub L: f64,
    /// ⌀: Diameter [m]
    pub D: f64,
    /// Diameter of bore hole [m]
    pub D_borehole: f64,
    /// Thickness of DHE pipe [m]
    pub thickness: f64,
    /// Thermal pipe resistance [Km/W]
    pub Ra: f64,
    /// Borehole thermal resistance [Km/W]
    pub Rb: f64,
    /// Thermal resistance [Km/W]
    pub R1: f64,
    pub fill_properties: FillProperties,
    pub brine_properties: BrineProperties,
    /// Mass throughput per DHE if pump is on [kg/s]
    pub Phi_m: f64,
    pub T_soil_0_parameters: TSoil0Parameters,
}

impl Default for DHE {
    fn default() -> Self {
        DHE {
            x: 0.0,
            y: 0.0,
            L: 100.0,
            D: 0.026,
            D_borehole: 0.115,
            thickness: 0.0,
            Ra: 0.0,
            Rb: 0.1,
            R1: 0.0,
            fill_properties: FillProperties::default(),
            T_soil_0_parameters: TSoil0Parameters::default(),
            brine_properties: BrineProperties::default(),
            Phi_m: 0.4,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "meta", derive(metastruct::MetaStruct))]
pub struct GlobalParameters {
    /// Number of sampling points in axial direction
    pub dim_ax: usize,
    /// Number of sampling points in radial direction
    pub dim_rad: usize,

    /// Soil layers
    pub soil_layers: Vec<SoilLayerProperties>,

    /// Radius of calculation [m]
    pub R: f64,
    /// Multiplier for the n_steps variable
    pub optimal_n_steps_multiplier: f64,
    /// Grid parameter for radial partition of domain of calculation
    pub Gamma: f64,
    /// Fraction of adiabatic boundary contitions
    pub adiabat: f64,
    /// Factor for n_steps
    pub n_steps_0: usize,
    /// Duration between two boundary condition refreshes [s]
    pub dt_boundary_refresh: f64,
    /// Sampling step [s]
    pub dt: f64,
    /// t0 [s]
    pub t0: f64,
    pub soil_parameters: SoilParameters,
}
impl Default for GlobalParameters {
    fn default() -> Self {
        Self {
            dim_ax: 4,
            dim_rad: 5,
            soil_layers: Vec::new(),
            R: 1.5,
            optimal_n_steps_multiplier: 2.0,
            Gamma: 2.0,
            adiabat: 0.0,
            n_steps_0: 4,
            dt_boundary_refresh: 7. * 24. * 3600.0,
            dt: 3600.0,
            t0: 0.0,
            soil_parameters: SoilParameters::default(),
        }
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename_all = "lowercase")]
pub enum TBrineCalcMethod {
    Dynamic,
    Stationary,
}
impl Default for TBrineCalcMethod {
    fn default() -> Self {
        Self::Dynamic
    }
}

pub type DHEConfiguration = DHEConfigurationGeneric<TBrineCalcMethod, GMethod>;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DHEConfigurationGeneric<Prm, G> {
    #[serde(flatten)]
    pub env: GlobalParameters,
    /// Method for computing g function
    pub g_method: G,
    /// Method for computing T_brine
    pub T_brine_method: Prm,
    /// Calculation Mode
    pub calculation_mode: CalculationMode,
    /// Input
    pub load: LoadType,
    /// Parameterset for each DHE
    pub dhe: Vec<DHE>,
}

impl Default for DHEConfiguration {
    fn default() -> Self {
        Self {
            env: GlobalParameters::default(),
            T_brine_method: TBrineCalcMethod::default(),
            calculation_mode: CalculationMode::P(PCalculationMode::default()),
            load: LoadType::default(),
            g_method: GMethod::default(),
            dhe: vec![DHE::default()],
        }
    }
}

fn verbose_io_err(f: &std::path::Path, e: std::io::Error) -> error::Error {
    format!("{}: {}", f.to_str().unwrap_or("<invalid encoding>"), e).into()
}

impl DHEConfiguration {
    pub fn from_file(f: &std::path::Path) -> Result<Self, error::Error> {
        let mut cfg: DHEConfiguration =
            serde_json::from_reader(std::fs::File::open(f).map_err(|e| verbose_io_err(f, e))?)?;
        if let LoadType::SampledLoad(sampled_load) = &mut cfg.load {
            sampled_load.load_file = combine_paths(f, &sampled_load.load_file)?;
        }
        Ok(cfg)
    }
    pub fn write_to_file(&self, f: &std::path::Path) -> Result<(), error::Error> {
        let mut cfg = self.clone();
        if let LoadType::SampledLoad(sampled_load) = &mut cfg.load {
            sampled_load.load_file = make_relative(
                f.parent().ok_or("File has no file part")?,
                &sampled_load.load_file,
            )?;
        }
        serde_json::to_writer_pretty(
            std::fs::File::create(f).map_err(|e| verbose_io_err(f, e))?,
            &cfg,
        )?;
        Ok(())
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "type", content = "arguments")]
pub enum CalculationMode {
    P(PCalculationMode),
}

pub trait Load {
    fn load(&self) -> Result<LoadData, error::Error>;
}

/// Input: P
#[derive(Clone, Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "meta", derive(metastruct::MetaStruct))]
pub struct PCalculationMode {
    /// Termination criterion for T_brine computation routine
    pub precision: f64,
}
impl Default for PCalculationMode {
    fn default() -> Self {
        Self { precision: 0.05 }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum LoadType {
    SampledLoad(SampledLoad),
    PMonthlyLoad(PMonthlyLoad),
}
impl Default for LoadType {
    fn default() -> Self {
        LoadType::SampledLoad(SampledLoad::default())
    }
}

pub type LoadData = [Vec<f64>; 2];

impl Load for SampledLoad {
    fn load(&self) -> Result<LoadData, error::Error> {
        use std::io::BufRead;
        let f =
            std::fs::File::open(&self.load_file).map_err(|e| verbose_io_err(&self.load_file, e))?;
        let reader = std::io::BufReader::new(f);
        let [t, X] = csv::parse_lines(reader.lines(), self.separator)?;
        Ok([t, X])
    }
}
fn combine_paths(
    root: &std::path::Path,
    path: &std::path::Path,
) -> Result<std::path::PathBuf, error::Error> {
    Ok(if path.is_relative() {
        root.with_file_name(".").join(path).canonicalize()?
    } else {
        path.to_path_buf()
    })
}

fn make_relative(
    root: impl AsRef<std::path::Path>,
    path: &std::path::Path,
) -> Result<std::path::PathBuf, error::Error> {
    if root.as_ref().is_relative() || path.is_relative() {
        return Err(error::Error("Both paths must be relative!".into()));
    }
    let mut rel_path = path.to_path_buf();
    for component in root.as_ref().components() {
        if let Ok(p) = rel_path.strip_prefix(component) {
            rel_path = p.to_path_buf();
        } else {
            rel_path = std::path::PathBuf::from("..").join(rel_path);
        }
    }
    Ok(rel_path)
}
impl Load for PMonthlyLoad {
    fn load(&self) -> Result<LoadData, error::Error> {
        const N_HOURS: usize = 24 * 365;
        let mut P = vec![0.; N_HOURS];
        let mut m = 0;
        let mut t_run = usize::min(self.t_run[m] as usize, 24);
        for (d, day_chunk) in P.chunks_exact_mut(24).enumerate() {
            if d >= month_start(m + 1) {
                m += 1;
                t_run = usize::min(self.t_run[m] as usize, 24);
            }
            for i in 0..t_run {
                day_chunk[i] = self.P_DHE;
            }
        }
        // Set the last n = self.Q_peak_feb hours of February to P = self.Q_peak_feb:
        const IDX_FEB: usize = 1;
        let h_feb_peak_end = 24 * month_start(IDX_FEB + 1);
        let h_feb_peak_start = h_feb_peak_end - f64::round(self.Delta_t_peak) as usize;
        for i in h_feb_peak_start..h_feb_peak_end {
            P[i] = self.Q_peak_feb;
        }
        Ok([crate::numerics::arange_n(0., 3600., N_HOURS), P])
    }
}
const MONTH_LEN: [u32; 12] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const fn month_start(i: usize) -> usize {
    if i == 0 {
        0
    } else {
        month_start(i - 1) + MONTH_LEN[i - 1] as usize
    }
}
impl Load for LoadType {
    fn load(&self) -> Result<LoadData, error::Error> {
        match self {
            LoadType::PMonthlyLoad(load) => load.load(),
            LoadType::SampledLoad(load) => load.load(),
        }
    }
}

/// Input: sampled load
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SampledLoad {
    /// Path to a csv file
    pub load_file: std::path::PathBuf,
    /// Separator
    #[serde(default = "default_separator")]
    pub separator: char,
}
const fn default_separator() -> char {
    ';'
}
impl Default for SampledLoad {
    fn default() -> Self {
        Self {
            load_file: std::path::PathBuf::default(),
            separator: default_separator(),
        }
    }
}

/// Input: monthly load
#[derive(Clone, Debug, Serialize, Deserialize, Default)]
#[cfg_attr(feature = "meta", derive(metastruct::MetaStruct))]
pub struct PMonthlyLoad {
    /// Heat extraction rate [W]
    pub P_DHE: f64,
    /// Peak of heat extraction rate at february [W]
    pub Q_peak_feb: f64,
    /// Duration of peak [h]
    pub Delta_t_peak: f64,
    /// For each month: number of hours, the DHE is running per day [h]
    pub t_run: [f64; 12],
}
