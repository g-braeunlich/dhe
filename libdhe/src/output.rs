use crate::{csv, table, CalcOutput, CalcPOutput};

impl CalcOutput {
    pub fn write_csv(&self, out: &mut impl std::io::Write) -> Result<(), crate::error::Error> {
        let values_tp: [&[f64]; 2] = [&self.t, &self.X];
        let tab = table::tab_join(
            table::ColTab::new(&values_tp as &[&[f64]]),
            table::iter_join(self.T.iter().map(CalcPOutput::row_iter)),
        );
        csv::save(
            vec!["t [s]".to_owned(), "P [W]".to_owned()]
                .into_iter()
                .chain(CalcOutput::csv_header(
                    self.dim_ax,
                    self.dim_rad,
                    self.T.len(),
                )),
            tab,
            out,
            ';',
        )?;
        Ok(())
    }
}
