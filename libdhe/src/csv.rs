use crate::error;
const N: usize = 2;
pub fn parse_lines<E: std::fmt::Display, T: Iterator<Item = Result<String, E>>>(
    mut lines: T,
    separator: char,
) -> Result<[Vec<f64>; N], error::Error> {
    let mut cols = [Vec::new(), Vec::new()];
    lines.next(); // Header
    for line in lines {
        let values = parse(&line?, separator)?;
        for (val, col) in values.iter().zip(&mut cols) {
            col.push(*val);
        }
    }
    Ok(cols)
}

fn parse(line: &str, separator: char) -> Result<[f64; N], error::Error> {
    let mut values = [0.; N];
    let mut index = 0;
    for val in line.split(separator) {
        if index == values.len() {
            return Err(format!("Expected {} values per line, got at least {}", N, N + 1).into());
        }
        values[index] = val.parse().map_err(|e| format!("{}: {}", e, val))?;
        index += 1;
    }
    if index < values.len() {
        return Err(format!("Expected {} values per line, only got {}", N, index).into());
    }
    Ok(values)
}

pub fn save<
    V: std::fmt::Display,
    I: Iterator<Item = V>,
    II: Iterator<Item = I>,
    S: std::fmt::Display,
    H: Iterator<Item = S>,
>(
    mut header: H,
    rows: II,
    out: &mut impl std::io::Write,
    separator: char,
) -> Result<(), String> {
    write_row(&mut header, out, separator)?;
    for mut row in rows {
        write_row(&mut row, out, separator)?;
    }
    Ok(())
}
fn write_row<V: std::fmt::Display, I: Iterator<Item = V>>(
    row: &mut I,
    out: &mut impl std::io::Write,
    separator: char,
) -> Result<(), String> {
    if let Some(cell) = row.next() {
        write!(out, "{}", cell).map_err(|e| format!("{}", e))?;
    }
    for cell in row {
        write!(out, "{}", separator).map_err(|e| format!("{}", e))?;
        write!(out, "{}", cell).map_err(|e| format!("{}", e))?;
    }
    writeln!(out).map_err(|e| format!("{}", e))?;
    Ok(())
}
