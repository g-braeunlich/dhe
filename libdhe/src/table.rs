pub struct IterJoin<II>(Vec<II>);
pub fn iter_join<V, I: Iterator<Item = V>, II: Iterator<Item = I>, III: Iterator<Item = II>>(
    iii: III,
) -> IterJoin<II> {
    IterJoin(iii.collect())
}

impl<V, I: Iterator<Item = V>, II: Iterator<Item = I>> Iterator for IterJoin<II> {
    type Item = std::iter::Flatten<std::vec::IntoIter<I>>;
    fn next(&mut self) -> Option<Self::Item> {
        let mut row: Vec<I> = Vec::new();
        for i_ in self.0.iter_mut() {
            if let Some(i) = i_.next() {
                row.push(i);
            } else {
                return None;
            }
        }
        Some(row.into_iter().flatten())
    }
}

pub struct TabJoin<T1, T2>(T1, T2);
pub fn tab_join<
    V,
    Row1: Iterator<Item = V>,
    Tab1: Iterator<Item = Row1>,
    Row2: Iterator<Item = V>,
    Tab2: Iterator<Item = Row2>,
>(
    t1: Tab1,
    t2: Tab2,
) -> TabJoin<Tab1, Tab2> {
    TabJoin(t1, t2)
}

impl<
        V,
        Row1: Iterator<Item = V>,
        Tab1: Iterator<Item = Row1>,
        Row2: Iterator<Item = V>,
        Tab2: Iterator<Item = Row2>,
    > Iterator for TabJoin<Tab1, Tab2>
{
    type Item = std::iter::Chain<Row1, Row2>;
    fn next(&mut self) -> Option<Self::Item> {
        match (self.0.next(), self.1.next()) {
            (Some(v1), Some(v2)) => Some(v1.chain(v2)),
            _ => None,
        }
    }
}

pub struct ColTab<V, Col: Iterator<Item = V>>(Vec<Col>);
impl<'a, V> ColTab<&'a V, std::slice::Iter<'a, V>> {
    pub fn new(cols: &'a [&'a [V]]) -> Self {
        Self(cols.iter().map(|c| (*c).iter()).collect())
    }
}
impl<V, Col: Iterator<Item = V>> Iterator for ColTab<V, Col> {
    type Item = std::vec::IntoIter<V>;
    fn next(&mut self) -> Option<Self::Item> {
        self.0
            .iter_mut()
            .map(|col| col.next())
            .collect::<Option<Vec<V>>>()
            .map(|v| v.into_iter())
    }
}
